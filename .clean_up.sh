#!/bin/sh 


# # Copy i3 configs
# cp -rf $HOME/.config/i3/ .

# # Copy nvim configs
# cp -rf $HOME/.config/nvim .

# # zshrc
# cp -f $HOME/.zshrc .

# # ranger
# cp -rf $HOME/.config/ranger .

dir=$HOME/.dotfiles                    # dotfiles directory

echo "Changing to the $dir directory... "
cd $dir
echo "...done!"

echo "Performing stow magic to remove dotfiles... "
stow -D . -t $HOME
echo "...done!"

# List of all packages
pacman -Qqe > packages
