export LF_ICONS="\
*.1=ℹ:\
*.7z=:\
*.R=📊:\
*.Rmd=📊:\
*.aac=:\
*.ace=:\
*.alz=:\
*.arc=:\
*.arj=:\
*.asf=:\
*.au=:\
*.avi=:\
*.bash=:\
*.bib=🎓:\
*.bmp=:\
*.bz2=:\
*.bz=:\
*.c=:\
*.cab=:\
*.cc=:\
*.cgm=:\
*.clj=:\
*.cmd=:\
*.coffee=:\
*.cpio=:\
*.cpp=:\
*.css=:\
*.csv=📓:\
*.d=:\
*.dart=:\
*.deb=:\
*.djvu=📚:\
*.dl=:\
*.dwm=:\
*.dz=:\
*.ear=:\
*.emf=:\
*.epub=📚:\
*.erl=:\
*.esd=:\
*.exs=:\
*.fish=:\
*.flac=ﱘ:\
*.flc=:\
*.fli=:\
*.flv=:\
*.fs=:\
*.ged=👪:\
*.gif=:\
*.gif=🖼:\
*.gl=:\
*.go=:\
*.gpg=🔒:\
*.gz=:\
*.h=:\
*.hh=:\
*.hpp=:\
*.hs=:\
*.html=:\
*.ico=🖼:\
*.img=📀:\
*.info=ℹ:\
*.iso=📀:\
*.jar=:\
*.java=:\
*.jl=:\
*.jpeg=:\
*.jpg=:\
*.js=:\
*.json=ﬥ:\
*.lha=:\
*.log=📙:\
*.lrz=:\
*.lua=:\
*.lz4=:\
*.lz=:\
*.lzh=:\
*.lzma=:\
*.lzo=:\
*.m2v=:\
*.m4a=:\
*.m4a=ﱘ:\
*.m4v=:\
*.md=:\
*.mid=:\
*.midi=:\
*.mjpeg=:\
*.mjpg=:\
*.mka=:\
*.mkv=辶:\
*.mng=:\
*.mov=:\
*.mp3=ﱘ:\
*.mp4=辶:\
*.mp4v=:\
*.mpc=:\
*.mpeg=:\
*.mpg=:\
*.nfo=ℹ:\
*.nix=:\
*.nuv=:\
*.oga=:\
*.ogg=:\
*.ogm=:\
*.ogv=:\
*.ogx=:\
*.opus=:\
*.or=❌:\
*.part=💔:\
*.pbm=:\
*.pcx=:\
*.pdf=:\
*.pgm=:\
*.php=:\
*.pl=:\
*.png=🖼:\
*.ppm=:\
*.pro=:\
*.ps1=:\
*.py=:\
*.qt=:\
*.r=📊:\
*.ra=:\
*.rar=:\
*.rar=📦:\
*.rb=:\
*.rm=:\
*.rmd=📊:\
*.rmvb=:\
*.rpm=:\
*.rs=:\
*.rz=:\
*.sar=:\
*.sass=🎨:\
*.scala=:\
*.sh=:\
*.spx=:\
*.svg=🗺:\
*.svgz=:\
*.swm=:\
*.t7z=:\
*.tar.gz=📦:\
*.tar=:\
*.taz=:\
*.tbz2=:\
*.tbz=:\
*.tex=📜:\
*.tga=:\
*.tgz=:\
*.tif=:\
*.tiff=:\
*.tlz=:\
*.torrent=🔽:\
*.ts=:\
*.txt=:\
*.txz=:\
*.tz=:\
*.tzo=:\
*.tzst=:\
*.vim=:\
*.vob=:\
*.war=:\
*.wav=ﱘ:\
*.webm=:\
*.wim=:\
*.wmv=:\
*.xbm=:\
*.xcf=:\
*.xcf=🖌:\
*.xlsx=📓:\
*.xml=📰:\
*.xpm=:\
*.xspf=:\
*.xwd=:\
*.xz=:\
*.yuv=:\
*.z=:\
*.zip=:\
*.zoo=:\
*.zsh=:\
*.zst=:\
di=:\
dt=:\
ex=🎯:\
fi=:\
ln=:\
or=:\
ow=📂:\
st=:\
tw=🤝:\
"
