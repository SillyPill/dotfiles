# wm independent hotkeys 

# terminal emulator
super + Return
	st

# terminal emulator at cwd (not very reliable)
super + shift + Return
	cd `xcwd` && st

# terminal emulator as a floating window
super + alt + Return
    bspc rule -a \* -o state=floating && st

# terminal emulator as a floating window at cwd (not reliable, again)
super + shift + alt + Return
    cd `xcwd` && bspc rule -a \* -o state=floating &&  st

# program launcher
alt + Return
	rofi -show drun

# make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd

#
# bspwm hotkeys
#

# quit/restart bspwm
super + alt + {q,r}
	bspc {quit,wm -r}

# Shutdown options
super + p
	dmenu_shutdown

# insert emoji
super + u
	dmenu_emoji_insert.sh a

# close and kill
super + {_,shift + }w
	bspc node -{c,k}

# alternate between the tiled and monocle layout
super + m
	bspc desktop -l next

# send the newest marked node to the newest preselected node
super + y
	bspc node newest.marked.local -n newest.!automatic.local

# swap the current node and the biggest node
super + g
	bspc node -s biggest

# Rotate
super + r
	bspc node @/ -R 90

# Flip Vertical
super + ctrl + shift + {l,h}
	bspc node @/ -F vertical

# Flip Horizontal
super + ctrl + shift + {k,j}
	bspc node @/ -F horizontal

#
# state/flags
#

# set the window state
super + {t,shift + t,s,f}
	bspc node -t {tiled,pseudo_tiled,floating,fullscreen}

# set the node flags
super + ctrl + {m,x,y,z}
	bspc node -g {marked,locked,sticky,private}

#
# focus/swap
#

# focus the node in the given direction
super + {_,shift + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

# focus the node for the given path jump
super + {p,b,comma,period}
	bspc node -f @{parent,brother,first,second}

# focus the next/previous node in the current desktop
super + {_,shift + }c
	bspc node -f {next,prev}.local

# focus the next/previous desktop in the current monitor
# super + {_,shift +}Tab
super + bracket{left,right}
	bspc desktop -f {prev, next}.occupied

# focus the last node/desktop
# super + bracket{left,right}
# 	bspc {node,desktop} -f last

# focus the older or newer node in the focus history
# super + {o,i}
# 	bspc wm -h off; \
# 	bspc node {older,newer} -f; \
# 	bspc wm -h on

# focus or send to the given desktop
super + {_,shift + }{1-9,0}
	bspc {desktop -f,node -d} '^{1-9,10}'

#
# preselect
#

# preselect the direction
super + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
	bspc node -o 0.{1-9}

# cancel the preselection for the focused node
super + ctrl + space
	bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

#
# move/resize
#

# USE SUPER + RIGHT CLICK TO RESIZE

# expand a window by moving one of its side outward
rsuper + alt + {h,j,k,l}
# 	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
# super + alt + shift + {h,j,k,l}
# 	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
super + {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0}

# Adjust Volume
XF86AudioRaiseVolume
	pactl set-sink-volume 0 +5% #increase sound volume

XF86AudioLowerVolume 
	pactl set-sink-volume 0 -5% #decrease sound volume

XF86AudioMute 
	pactl set-sink-mute 0 toggle # mute sound

# Adjust brightness
XF86MonBrightnessUp 
	brightnessctl set 5%+
XF86MonBrightnessDown 
	brightnessctl set 5%-

# MPD Bindings
XF86AudioNext
	mpc next 

XF86AudioPrev
	mpc prev 

XF86AudioPlay
	mpc toggle

XF86AudioPause
	mpc pause

# Screenshot
Print 
	maim -u "$HOME/Screenshots/$(date +%s).png" && notify-send "Screenshot Captured"

super + Print 
    maim -u -i $(xdotool getactivewindow) "$HOME/Screenshots/$(date +%s).png" && notify-send "Screenshot Captured"

super + shift + Print 
	maim -u -s "$HOME/Screenshots/$(date +%s).png" 

# OCRing
super + alt + Print
    ocr.sh
	
# Clipmenu Binding
ctrl + shift + b
	CM_LAUNCHER=rofi clipmenu -i -matching fuzzy -p "select"

# Plumb
super + e
	plumb

# Move windown to next desktop
super + shift + m
	bspc node -m next 

# temp
super + shift + p
    t_ss_p.sh
