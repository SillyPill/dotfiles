-- Most keybinds are set along with the plugin configuration
--
vim.keymap.set('n', '<C-\\>', vim.cmd.Vexplore)
vim.keymap.set('n', '<leader>Z', ':w<cr>')

vim.keymap.set('n', '<C-w>\\', ':resize<CR>:vertical res<CR>')
vim.keymap.set('n', '<C-l>', function()
  vim.cmd.nohlsearch()
  vim.cmd.cclose()
end)
