return {
  'Vigemus/iron.nvim',
  config = function()
    local iron = require 'iron.core'

    iron.setup {
      config = {
        -- Whether a repl should be discarded or not
        scratch_repl = true,
        -- Your repl definitions come here
        repl_definition = {
          sh = {
            -- Can be a table or a function that
            -- returns a table (see below)
            command = { 'zsh' },
          },
          python = {
            command = { 'ipython', '--no-autoindent' },
            format = require('iron.fts.common').bracketed_paste_python,
          },
        },
        -- How the repl window will be displayed
        -- See below for more information
        repl_open_cmd = require('iron.view').split.vertical.botright(50),
      },
      -- Iron doesn't set keymaps by default anymore.
      -- You can set them here or manually add keymaps to the functions in iron.core
      keymaps = {
        clear = '<leader>scl',
        cr = '<leader>s<cr>',
        exit = '<leader>sq',
        interrupt = '<leader>s<space>',
        mark_motion = '<leader>mc',
        mark_visual = '<leader>mc',
        remove_mark = '<leader>md',
        send_file = '<leader>sf',
        send_line = '<leader>scc',
        send_mark = '<leader>sm',
        send_motion = '<leader>sc',
        visual_send = '<leader>sc',
      },
      -- If the highlight is on, you can change how it looks
      -- For the available options, check nvim_set_hl
      highlight = {
        italic = true,
      },
      ignore_blank_lines = false, -- ignore blank lines when sending visual select lines
    }

  end,

  -- For lazy loading. Keymaps have to be defined twice
  cmd = {
    'IronRepl',
    'IronReplHere',
    'IronRestart',
    'IronSend',
    'IronFocus',
    'IronHide',
    'IronWatch',
    'IronAttach',
  },
  keys = {
    '<leader>scl',
    '<leader>s<cr>',
    '<leader>sq',
    '<leader>s<space>',
    '<leader>mc',
    '<leader>mc',
    '<leader>md',
    '<leader>sf',
    '<leader>scc',
    '<leader>sm',
    '<leader>sc',
    '<leader>sc',
    { '<space>rs', '<cmd>IronRepl<cr>' },
    { '<space>rr', '<cmd>IronRestart<cr>' },
    { '<space>rf', '<cmd>IronFocus<cr>' },
    { '<space>rh', '<cmd>IronHide<cr>' },
  },
}
