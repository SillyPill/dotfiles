#!/usr/bin/sh

curl -L 'http://unicode.org/Public/emoji/latest/emoji-test.txt' |\
    sed -e '/^#.*/d' `# Delete all comments`\
        -e '/^\s*$/d' `# Delete all empty lines`\
        -e 's/\s\+/ /g' `# Remove extra whitespace`\
        -e 's/E[0-9]\+\.[0-9]\+/;/' `# Delete ugly emoji version and replace with ';'`\
        -e 's/#/;/' `# replace '#' with ';'`\
        -e 's/\([A-F0-9]\+\)/U+\1/g' `# Delete the fully-qualified feild`\
        -e '/^.*minimally-qualified.*$/d' `# No minimally-qualified emoji`\
        -e '/^.*unqualified.*$/d' `# No unqualified emoji`\
        -e '/^.*component.*$/d' `# No emoji component`\
        -e 's/fully-qualified ; //g'\
