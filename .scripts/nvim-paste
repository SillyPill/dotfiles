#!/usr/bin/sh

# Default print type
print_type="markdown"
path="./"

# Function to print the final statement based on print type
print_statement() {
	case "$print_type" in
	"markdown") echo "![$1]($1)" ;;
	"html") echo "$1" ;;
	*) echo "($1)" ;;
	esac
}

# Function to display help text
display_help() {
	echo "Usage: $0 [--print-type <type>] [--path <type>] [--help]"
	echo "Options:"
	echo "  --print-type <type>  Set the format of the final print statement. Valid values: default, uppercase, lowercase."
    echo "  --path <path>  attachment path (default is \"./\")"
	echo "  --help               Display this help text."
}

# Get options
while [ $# -gt 0 ]; do
	case "$1" in
	--print-type)
		shift
		if [ $# -gt 0 ]; then
			print_type="$1"
		else
			echo "Error: --print-type option requires a value."
			exit 1
		fi
		;;
	--path)
		shift
		if [ $# -gt 0 ]; then
			path="$1"
		else
			echo "Error: --path option requires a value."
			exit 1
		fi
		;;
	--help)
		display_help
		exit 0
		;;
	*)
		echo "Unknown option: $1"
		exit 1
		;;
	esac
	shift
done

# Get the MIME types from clipboard contents
mime_types=$(wl-paste -l)

# Extract the MIME type from the first line
mime_type=$(echo "$mime_types" | head -n 1)

# Generate a unique filename based on the current timestamp
filename="$(date +%Y-%m-%d%H-%M-%S)"

# Extract the file extension from the MIME type
case $mime_type in
image/png) extension=".png" ;;
image/jpeg) extension=".jpg" ;;
application/pdf) extension=".pdf" ;;
text/*) extension=".txt" ;;
*) extension=$(echo "$mime_type" | awk -F'/' '{print $2}') ;;
esac

# If the extension is empty or contains invalid characters, use a default one
if [ -z "$extension" ] || echo "$extension" | grep -Eq '[\ /:*?"<>|]'; then
	extension=".dat"
fi

filepath="${path}/${filename}${extension}"

# Create the new file with the determined extension
wl-paste >"${path}/${filename}${extension}"

print_statement "$filepath"
